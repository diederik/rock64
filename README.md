# Rock64

This repo contains artifacts to get pure Debian working on Pine64's Rock64 SBC.  

## Making an image

There is now a `Makefile` which uses the `rock64-master.yaml` file to create other `yaml` files which in turn are used to create `.img` files by feeding it to `vmdb2`.  
Running (just) **`make`** will print out usage information with some examples.

For easy reference:

```
make bullseye.yaml
make bookwork.img
```
The first command makes a `yaml` file from which to create a *Bullseye* image.  
The second command makes an `img` file with a *Bookworm* system (by creating the corresponding `yaml` file first).

The `rootfs` directory contains files which will be copied into the image to add/improve functionality of the system.

## The 'kernel/patches' directory

This directory contains patches which I have applied to *upstream*'s kernel in order to better support the functionality of the Rock64 device.  
I've now also 'converted' some patches which can be applied to *Debian*'s kernel. I have or plan to submit them to Debian's kernel team so everyone can benefit from them.

Note that most ideas or whole patches are not my work, but primarily [*LibreELEC*'s work](https://github.com/LibreELEC/LibreELEC.tv/tree/master/projects/Rockchip/patches/linux/default).  
Some are taken from upstream and others have (partly) been submitted to upstream.  
Adaptation to Debian probably is my work.
<br />
<br />
<br />
That's one of the reason I like FLOSS: Working together to make it better for everyone :-)

