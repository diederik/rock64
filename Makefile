SBC := rock64
SBC_PREFIX := $(SBC)_
MASTER_YAML := $(SBC)-master.yaml

RELEASES := bullseye bookworm

# This variable has the same value as RELEASES and as such doesn't provide a benefit, yet.
# But if you want to create variants like with Raspberry Pi, then it will.
# platforms := $(foreach plat, $(FAMILIES),$(foreach rel, $(RELEASES), rpi_$(plat)_$(rel)))
platforms := $(foreach rel, $(RELEASES), $(rel))

target_platforms:
	@echo $(platforms)


clean: _clean_yaml _clean_tarballs _clean_logs _clean_images
yaml: $(addsuffix .yaml, $(platforms))
images: $(addsuffix .img, $(platforms))


# enable security repo for the Stable release
bullseye.yaml: $(MASTER_YAML)
	cat $(MASTER_YAML) | \
	sed "s/__SBC__/$(SBC)/" | \
	sed "s/__RELEASE__/bullseye/" | \
	sed -r "s/#(deb http:\/\/security.debian.org)/\1/" > $(addprefix $(SBC_PREFIX), $@)

bookworm.yaml: $(MASTER_YAML)
	cat $(MASTER_YAML) | \
	sed "s/__SBC__/$(SBC)/" | \
	sed "s/__RELEASE__/bookworm/" > $(addprefix $(SBC_PREFIX), $@)


%.img: imgname = $(addprefix $(SBC_PREFIX), $@)
# vmdb2 must be run as root
%.img: _check_root %.yaml
	time nice vmdb2 --verbose --rootfs-tarbal=$(subst .img,.tar.gz,$(imgname)) --log=$(subst .img,.log,$(imgname)) --output=$(imgname) $(subst .img,.yaml,$(imgname))
	chmod 0644 $(imgname) $(@, .img = .log)


# helper targets
_check_root:
ifneq ($(shell whoami), root)
	$(error "This must be run as root")
endif

# internal clean up targets
_clean_yaml:
	rm -f $(addprefix $(SBC_PREFIX), $(addsuffix .yaml, $(platforms)))

_clean_tarballs:
	rm -f $(addprefix $(SBC_PREFIX), $(addsuffix .tar.gz, $(platforms)))

_clean_logs:
	rm -f $(addprefix $(SBC_PREFIX), $(addsuffix .log, $(platforms)))

_clean_images:
	rm -f $(addprefix $(SBC_PREFIX), $(addsuffix .img, $(platforms)))


usage:
	@echo "Usage: 'make <release>.<type>' where"
	@echo "<release> is 'bullseye' or 'bookworm' and"
	@echo "<type> is 'yaml' or 'img'"
	@echo
	@echo "Example 1: 'make bullseye.yaml'"
	@echo "This creates a $(SBC_PREFIX)bullseye.yaml file from which an image can be created"
	@echo
	@echo "To create all the yaml files, do 'make yaml'"
	@echo
	@echo "Example 2: 'make bookworm.img'"
	@echo "This creates a $(SBC_PREFIX)bookworm.img file by generating the yaml file and pass that onto vmdb2"
	@echo
	@echo "To create all the images, do 'make images'"

.DEFAULT_GOAL := usage
